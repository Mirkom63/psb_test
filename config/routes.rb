Rails.application.routes.draw do

  	devise_for :users
	#mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

	get '/choose' => 'index#choose'
	get '/test/:id_hero/:count_test' => 'test#index'
	get '/test_mobile/:id_hero/:count_test' => 'test#index_mobile'
	get '/test_finish' => 'test#finish'

	get '/share/:id' => 'index#share'



	post '/check_test' => 'test#check_test'
	post '/send_form' => 'test#send_form'


	root "index#index"
	# For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
