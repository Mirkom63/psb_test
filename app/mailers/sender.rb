class Sender < ApplicationMailer

	default from: 'info@go2gos.ru'

	def send_form(email,code)

		@code=code

		mail(to: email, subject: "Инструкция по активации кода")

	end

end
 
