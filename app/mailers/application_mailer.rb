class ApplicationMailer < ActionMailer::Base
  default from: 'info@go2gos.ru'
  layout 'mailer'
end
