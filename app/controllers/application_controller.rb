class ApplicationController < ActionController::Base

	before_action :check_auth?

  	def check_auth?

      if not params[:action]=='share'
    		if not user_signed_in?
    			new_user=User.new
    			new_user.email="user_#{Time.now.to_i}@example.com"
    			new_user.ip=request.remote_ip
          new_user.send_book=false
    			new_user.save!(validate: false)

    			sign_in_and_redirect new_user, :event => :authentication 

    		end
      end

  	end


	def after_sign_in_path_for(resource)
    if params[:action]=='choose'
      '/choose'
    else
      '/'
    end
    
	end


end
