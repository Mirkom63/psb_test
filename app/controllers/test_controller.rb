class TestController < ApplicationController

	
	def send_form

		code=Code.find_by(use: false)
		if code.present?
			code.use=true
			code.save

			current_user.email_form=params[:email]
			current_user.vk=params[:vk]
			current_user.send_book=true
			current_user.code=code.code
			current_user.save(validate: false)

			Sender.send_form(params[:email],code.code).deliver_now
		end



		render plain: 'ok'
	end

	def check_test

		current_user["test_#{params[:test_position]}"]=params[:test]
		

		link=''
		id_test=params[:test_position].to_i
		if id_test<5
			next_id=id_test+1

			if DeviceDetector.new(request.user_agent).device_type=='smartphone' 
				link="/test_mobile/#{current_user.hero.id}/#{next_id}"
			else
				link="/test/#{current_user.hero.id}/#{next_id}"
			end 


			

		else
			link="/test_finish"

		
			level=nil

			count_big=0
			count_middle=0
			count_small=0

			1.upto(5).each do |i|

				test=HeroTestValue.find(current_user["test_#{i}"])

				if test.big==true
					count_big=count_big+1
				end
				if test.middle==true
					count_middle=count_middle+1
				end
				if test.small==true
					count_small=count_small+1
				end
			end	


			ar_result=[count_big,count_middle,count_small].sort.reverse

			if ar_result[0]==count_small
				level=3
			end
			if ar_result[0]==count_middle
				level=2
			end
			if ar_result[0]==count_big
				level=1
			end

			company=Company.where(level: level).order("RAND()").first
	
			current_user.company_id=company.id
			current_user.type_description=rand(0..1)


		
		end

		
		
		current_user.save(validate: false)
		redirect_to link
		
		


		
	end

	def finish

		if current_user.type_description.blank? or current_user.company_id.blank? or current_user.test_1.blank? or current_user.test_2.blank? or current_user.test_3.blank? or current_user.test_4.blank? or current_user.test_5.blank?

			redirect_to '/'

		end

	end

	def index

		@hero=Hero.find(params[:id_hero])
		@test=@hero.hero_test.find_by(position: params[:count_test])

		if current_user.hero_id.blank?
			current_user.hero_id=@hero.id
			current_user.save(validate: false)
		end

		

	end


	def index_mobile

		@hero=Hero.find(params[:id_hero])
		@test=@hero.hero_test.find_by(position: params[:count_test])

		if current_user.hero_id.blank?
			current_user.hero_id=@hero.id
			current_user.save(validate: false)
		end

		

	end
end
