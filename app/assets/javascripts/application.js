// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs

//= require plugin/ui.js
//= require plugin/owl.carousel.js




$(document).ready(function(){


	$('.close_info').click(function(){
		$('.pop-up-info-hero').hide();
	});

	$('.hero_info_icon').click(function(){
		if($('.pop-up-info-hero').css('display')=='none'){
			$('.pop-up-info-hero').show();
		}else{
			$('.pop-up-info-hero').hide();
		}
	});

	$(document).mouseup(function (e){ // событие клика по веб-документу
		var div = $(".pop-up-info-hero,.hero_info_icon"); // тут указываем ID элемента
		if (!div.is(e.target) // если клик был не по нашему блоку
		    && div.has(e.target).length === 0) { // и не по его дочерним элементам
			$('.pop-up-info-hero').hide(); // скрываем его
		}
	});



	$('.close_registration').click(function(){
		$('.pop-up-registration').fadeOut();
	});
	$('.button_how_work').click(function(){
		$('.pop-up-registration').fadeIn();
	});

	$('.pop_up_form_close').click(function(){
		$('.pop_up_form').hide();
	});
	$('.button_open_form').click(function(){
		$('.pop_up_form').show();
	});

	$('input').keyup(function(){
		if($('body').width()==320){
			$(this).css('background','#FFF');
		}else{
			$(this).css('background','#e4eef6');
		}

	});

	$('.js-form').submit(function(e){
		e.preventDefault();
		var send_form=true;

		var vk=$('.js-vk').val();
		var email=$('.js-email').val();

		if(email.length==0){
			send_form=false;
			$('.js-email').css('background','tomato');
		}

		if(vk.length==0){
			send_form=false;
			$('.js-vk').css('background','tomato');
		}else{

			if($('.js-vk').val().indexOf('vk.com/')==-1 && $('.js-vk').val().indexOf('facebook.com/')==-1) {
				send_form=false;
				$('.js-vk').css('background','tomato');
			}

		}




		if(send_form==true){

			$('.thanks_form').show();
        	$('.js-form').hide();

			$.ajax({

		        type     :'POST',
		        cache    : false,
		        data: $(this).serialize(),
		        url  : '/send_form',
		        success  : function(response) {


		        },
		        error: function(jqxhr, status, errorMsg) {


		        }

		    });


		}
	});

	$('.block_test_info input[type="radio"]').checkboxradio();

	$('.block_test_info input[type="radio"]').change(function(){

		$('.button_next_step').slideDown();
	});

	if($('body').width()==320){
		setTimeout(function(){
			$('.mobile_block_index').css('opacity','1');
		},1600);

		$('.list_hero').owlCarousel({
	          items:2,
	          loop:true,
	          dots: false,
	          //nav: true,
	          autoWidth:true,

	    });

	    var open=false;

	    $(document).on('click','.owl-item.active:first',function(){
	    	if(open==false){
	    		$('.point_hero_mobile').addClass('point_hero_mobile_hide');
		 		$(this).find('.point_hero_mobile').addClass('point_hero_mobile_active').removeClass('point_hero_mobile_hide');
		 		$('.owl-nav').fadeOut();
		 		setTimeout(function(){
		 			open=true;
		 		},300);
	    	}

	 	});

	 	$(document).on('click','.button_hero_hover_back',function(){
	 		if(open==true){
		 		$('.point_hero_mobile').removeClass('point_hero_mobile_hide');
		 		$('.point_hero_mobile').removeClass('point_hero_mobile_active');
		 		$('.owl-nav').fadeIn();
		 		setTimeout(function(){
		 			open=false;
		 		},300);
		 	}
	 	});

	}







});
