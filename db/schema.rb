# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_02_27_164517) do

  create_table "codes", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "code"
    t.boolean "use"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "companies", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.string "level"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "hero_test_values", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.boolean "big"
    t.boolean "middle"
    t.boolean "small"
    t.bigint "hero_test_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["hero_test_id"], name: "index_hero_test_values_on_hero_test_id"
  end

  create_table "hero_tests", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.string "position"
    t.bigint "hero_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["hero_id"], name: "index_hero_tests_on_hero_id"
  end

  create_table "heros", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.text "post"
    t.string "site"
    t.string "company_name"
    t.text "description"
    t.text "history"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "email_form"
    t.string "vk"
    t.boolean "send_book"
    t.string "email", default: "", null: false
    t.string "phone"
    t.string "ip"
    t.string "encrypted_password", default: "", null: false
    t.bigint "hero_id"
    t.integer "test_1"
    t.integer "test_2"
    t.integer "test_3"
    t.integer "test_4"
    t.integer "test_5"
    t.bigint "company_id"
    t.integer "type_description"
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "code"
    t.index ["company_id"], name: "index_users_on_company_id"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["hero_id"], name: "index_users_on_hero_id"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "hero_test_values", "hero_tests"
  add_foreign_key "hero_tests", "heros"
  add_foreign_key "users", "companies"
  add_foreign_key "users", "heros"
end
