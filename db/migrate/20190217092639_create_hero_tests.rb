class CreateHeroTests < ActiveRecord::Migration[5.2]
  def change
    create_table :hero_tests do |t|
    	t.string :name
    	t.string :position
    	t.references :hero, index: true, foreign_key: true
      	t.timestamps	
    end
  end
end
