class AddToUserVk < ActiveRecord::Migration[5.2]
  def change
  	add_column :users, :email_form, :string
  	add_column :users, :vk, :string
  end
end
