class CreateHeros < ActiveRecord::Migration[5.2]
  def change
    create_table :heros do |t|
    	t.string :name
    	t.string :company_name
    	t.text :description
    	t.text :history
      	t.timestamps
    end
  end
end
