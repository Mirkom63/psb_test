class AddToUserCompany < ActiveRecord::Migration[5.2]
  def change
  	add_column :users, :type_description, :integer
  	add_reference :users, :company, index: true, foreign_key: true
  end
end
