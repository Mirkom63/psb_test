class CreateHeroTestValues < ActiveRecord::Migration[5.2]
  def change
    create_table :hero_test_values do |t|
    	t.string :name
    	t.boolean :big
    	t.boolean :middle
    	t.boolean :small
    	t.references :hero_test, index: true, foreign_key: true
      	t.timestamps
    end
  end
end
